-- Main configuration file. It only serves to load the relevant files in this case
-- Order by which the files are required is important
require('functions')
require('set_config')
require('autocmd')
require('legacy_vim')
require('keybindings')
