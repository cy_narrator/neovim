-- Remember that the key, which is on the left side should always be in brackets([]), thats just how lua works
-- All keybindings are set as noremap which can be changed inside functions.lua
-- Works for insert, visual and normal mode
all_modes_remap_list = {

}

insert_remap_list = {
    ['<C-s>'] = '<Esc>:wq<CR>i'
}

normal_remap_list = {
    ['k'] = 'gk',
    ['j'] = 'gj',
    ['0'] = 'g0',
    ['^'] = 'g^',
    ['$'] = 'g$',
    ['<C-s>'] = '<Esc>:wq<CR>'
}

visual_remap_list = {
    ['>'] = '>gv',
    ['<'] = '<gv'
}

-- Loops to read those keybindings and register them to nvim
for key,value in pairs(all_modes_remap_list) do
    my_mapall(key, value)
end

for key,value in pairs(normal_remap_list) do
    my_nmap(key, value)
end

for key,value in pairs(visual_remap_list) do
    my_vmap(key, value)
end
