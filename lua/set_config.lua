-- The key(aka the one on the left) should always be inside a bracket([]). Thats just how Lua works

let_list = {
    ['mapleader'] = "' '", -- Need to have "' '" otherwise the inner ' ' will be ignored and will give errors
}

set_list = {
    'number', 
    'expandtab', 
    'smarttab', 
    'autoindent', 
    'hidden', 
    'ruler', 
    'showcmd', 
    'relativenumber',
    'splitbelow',
    'splitright',
    'exrc',
    ['mouse'] = 'n',
    ['bs'] = "2", -- Its weird that this expects string even though it should have been an integer, but thats okay
    ['tabstop'] = 4,
}

for key, value in pairs(let_list) do
    my_let(key, value)
end

for key, value in pairs(set_list) do
    if type(key) == "number" then
        -- Lua tables are supposed to be in key value pair, but if it is just a single value, the key is its index starting at 1
        -- Essentially checking if the item is in key-value form or just a value
        my_set(value, true)
    else
        my_set(key,value)
    end
end