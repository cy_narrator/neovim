-- my_autocmd({BufNewFile, BufRead}, {'.py', '.html'}, print('Hi there'), nil)
-- List of strings to represent names of autocmd group
autogroup_list = {
    "SpellCheck"
}

autocmd_list = {
    [{"BufRead", "BufNewFile"}] = { pattern = { "*.txt", "*.md", "*.tex" }, command = "setlocal spell", group = "SpellCheck" }

}

for autogroup_list_index = 1, #autogroup_list do
    my_autocmd_group(autogroup_list[autogroup_list_index])
end

for autocmd_event, autocmd_action in pairs(autocmd_list) do
    my_autocmd(autocmd_event, autocmd_action)
end

