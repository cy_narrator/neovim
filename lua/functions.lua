-- All the functions that are to be used shall be defined here
-- There is whole bunch of 'my' to namespace and ensure conflict does not occour with built in functions


function my_map(mode, shortcut, command)
  vim.api.nvim_set_keymap(mode, shortcut, command, { noremap = true, silent = false }) -- Setting silent to true will suppress error or warning messages
end

function my_autocmd_group(name)
  vim.api.nvim_create_augroup(name, {clear = true})
end

function my_autocmd(event, action)
  vim.api.nvim_create_autocmd(event, action)
end

function my_vim_cmd(command_str)
  vim.cmd(command_str)
end

function my_nmap(shortcut, command)
  my_map('n', shortcut, command)
end

function my_imap(shortcut, command)
  my_map('i', shortcut, command)
end

function my_vmap(shortcut, command)
  my_map('v', shortcut, command)
end

function my_cmap(shortcut, command)
  my_map('c', shortcut, command)
end

function my_mapall(shortcut, command)
  my_nmap(shortcut,command)
  my_imap(shortcut,command)
  my_vmap(shortcut,command)
end

-- Set related functions
function my_set(command_str, value)
    vim.opt[command_str] = value
end

function my_let(key, value)
  my_vim_cmd("let " .. key .. " = " .. value)
end

